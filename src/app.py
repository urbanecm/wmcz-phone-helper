#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import yaml
from datetime import datetime
import random
import requests
from flask import redirect, request, render_template, url_for, flash, session, make_response
from flask import Flask
from flask_jsonlocale import Locales
from flask_mwoauth import MWOAuth
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

app = Flask(__name__, static_folder='../static')

# Load configuration from YAML file
__dir__ = os.path.dirname(__file__)
app.config.update(
    yaml.safe_load(open(os.path.join(__dir__, os.environ.get(
        'FLASK_CONFIG_FILE', 'config.yaml')))))

# Add databse credentials to config
if app.config.get('DBCONFIG_FILE') is not None:
    app.config['SQLALCHEMY_DATABASE_URI'] = app.config.get('DB_URI') + '?read_default_file={cfile}'.format(cfile=app.config.get('DBCONFIG_FILE'))

db = SQLAlchemy(app)
migrate = Migrate(app, db)

mwoauth = MWOAuth(
    consumer_key=app.config.get('CONSUMER_KEY'),
    consumer_secret=app.config.get('CONSUMER_SECRET'),
    base_url=app.config.get('OAUTH_MWURI'),
    return_json=True
)
app.register_blueprint(mwoauth.bp)

locales = Locales(app)
_ = locales.get_message

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(255), nullable=False)
    is_active = db.Column(db.Boolean, server_default='1', nullable=False)
    is_admin = db.Column(db.Boolean, server_default='0', nullable=False)
    phone_number = db.Column(db.String(255), server_default='', nullable=False)
    online = db.Column(db.Boolean, server_default='0')

class Call(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    datetime = db.Column(db.String(255))
    direction = db.Column(db.String(255))
    caller = db.Column(db.String(255))
    receiver = db.Column(db.String(255))

def logged():
    return mwoauth.get_current_user() is not None

def get_user():
    return User.query.filter_by(
        username=mwoauth.get_current_user()
    ).first()

@app.context_processor
def inject_base_variables():
    return {
        "logged": logged(),
        "user": get_user(),
        "username": mwoauth.get_current_user(),
    }

@app.route('/')
def index():
    if not logged():
        return render_template('login.html')
    
    user = get_user()
    if not user:
        return render_template('no_user.html')
    return render_template('index.html')

@app.route('/update_options', methods=['POST'])
def update_options():
    user = get_user()
    if not user:
        return 'invalid'
    
    user.phone_number = request.form.get('phone-number')
    user.online = request.form.get('online') is not None
    db.session.add(user)
    db.session.commit()
    flash('Successfully saved')
    return redirect(url_for('index'))

@app.route('/order-callback', methods=['POST'])
def order_callback():
    user = get_user()
    if not user:
        flash('Not logged in', 'error')
        return redirect(url_for('index'))

    recipient = request.form.get('recipient')
    if recipient is None:
        flash('No recipient passed', 'error')
        return redirect(url_for('index'))

    requests.post(
        'https://www.odorik.cz/api/v1/callback',
        data={
            'user': app.config.get('PHONE_LINE_ID'),
            'password': app.config.get('PHONE_LINE_PWD'),
            'simple': 'true',
            'caller': user.phone_number,
            'recipient': recipient
        }
    )

    call = Call()
    call.direction = 'outgoing'
    call.caller = user.phone_number
    call.receiver = recipient
    call.datetime = datetime.now().strftime('%Y%m%d%H%M%S')
    db.session.add(call)
    db.session.commit()

    flash('Callback successfully ordered')
    return redirect(url_for('index'))

@app.route('/api/route-caller')
def route_caller():
    def generic_response(text, status):
        response = make_response(text, status)
        response.mimetype = 'text/plain'
        return response
    
    def unavailable_response():
        return generic_response(
            'answer\nplay:http://www.odorik.cz/w/_media/ivr:hallo.sln\nhangup',
            200
        )
    
    def redirect_response(phone_number):
        print('Redirecting to %s' % phone_number)
        return generic_response(
            'answer\nsetclip:%s\ndial:%s' % (app.config.get('APP_PHONENO'), phone_number),
            200
        )
        
    caller = request.args.get('from')
    receiver = request.args.get('to')
    line = request.args.get('line')

    online_users = User.query.filter_by(online=True).all()
    if len(online_users) == 0:
        return unavailable_response()

    winner = random.choice(online_users)

    call = Call()
    call.caller = caller
    call.direction = 'incoming'
    call.receiver = winner.phone_number
    call.datetime = datetime.now().strftime('%Y%m%d%H%M%S')
    db.session.add(call)
    db.session.commit()

    return redirect_response(winner.phone_number)

if __name__ == "__main__":
    app.run(debug=True, threaded=True)
